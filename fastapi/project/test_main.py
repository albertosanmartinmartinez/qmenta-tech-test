from fastapi.testclient import TestClient

from project.main import app


client = TestClient(app)

def test_date_ok():
    response = client.get("/button1")

    assert response.status_code == 200

def test_name_ok():
    response = client.get("/button2")

    assert response.status_code == 200

def test_name_ko_not_file():
    response = client.get("/button2?file_name=wqieofhiufh")
    
    assert response.status_code == 500
    assert 'No such file or directory' in response.json()['detail']

def test_name_ko_not_attribute():
    response = client.get("/button2?attribute_name=ioqhfrio3r4")
    
    assert response.status_code == 500
    assert 'No such attribute name' in response.json()['detail']