import pydicom
import datetime
import logging

from logging.config import dictConfig

from fastapi import FastAPI, HTTPException
from fastapi.middleware.cors import CORSMiddleware
from fastapi.responses import JSONResponse

# comment next line for run tests
from log_config import logging_config

# uncomment next line for run tests
#from .log_config import logging_config


dictConfig(logging_config)
logger = logging.getLogger("qmenta_logging")

app = FastAPI()

origins = [
    "http://0.0.0.0:3000"
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_methods=["*"],
    allow_headers=["*"],
)


@app.get("/button1")
async def button1(date_format='%d-%m-%Y'):

    try:
        current_date = datetime.datetime.now().strftime(date_format)
        return JSONResponse(content=current_date)
        
    except Exception as e:
        logger.error(str(e))
        raise HTTPException(status_code=500, detail=str(e))


@app.get("/button2")
async def button2(file_name='MR000001', attribute_name='PatientName'):

    try:
        dataset = pydicom.dcmread(f'files/{file_name}')
    
    except Exception as e:
        logger.error(str(e))
        raise HTTPException(status_code=500, detail=str(e))
    
    patient_name = dataset.data_element(attribute_name)

    if patient_name is None:
        logger.error('No such attribute name')
        raise HTTPException(status_code=500, detail="No such attribute name")
    
    return JSONResponse(content=str(patient_name.value))
