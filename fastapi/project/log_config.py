LOG_FORMAT: str = "%(levelprefix)s %(asctime)s | %(message)s"
LOG_LEVEL: str = "DEBUG"

logging_config = {
    "version": 1,
    "formatters": {
        "default": {
            "()": "uvicorn.logging.DefaultFormatter",
            "format": LOG_FORMAT
        },
    },
    "handlers": {
        "console": {
            "formatter": "default",
            "class": "logging.StreamHandler",
            "stream": "ext://sys.stderr",
            "level": LOG_LEVEL
        },
    },
    "loggers": {
        "qmenta_logging": {
            "handlers": ["console"],
            "level": LOG_LEVEL,
            "propagate": True,
        },
    }
}