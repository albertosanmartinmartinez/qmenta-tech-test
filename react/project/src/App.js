import './App.css';
import React, { useState } from "react"

function App() {
  
  let [current_date, setCurrentDate] = useState([])
  let [patient_name, setPatientName] = useState([])

  let api_url = "http://0.0.0.0:8000/";
  let headers = {
    "content-type": "application/json",
    "accept": "application/json",
    "Access-Control-Allow-Origin": "*"
  };

  function button1() {

    fetch(api_url + "button1", {
      "method": "GET",
      "headers": headers
    })
    .then((response) => {
      if(response.status === 200) {
        return response.json()
      }
      throw new Error('Something went wrong!');
    })
    .then((result) => {
      //console.log(result);
      setCurrentDate(result);
    })
    .catch((error) => {
      console.log(error);
    });
  }

  function button2() {

    fetch(api_url + "button2", {
      "method": "GET",
      "headers": headers
    })
    .then((response) => {
      if(response.status === 200) {
        return response.json()
      }
      throw new Error('Something went wrong!');
    })
    .then((result) => {
      //console.log(result);
      setPatientName(result);
    })
    .catch((error) => {
      console.log(error);
    });
  }

  return (
    <div className="row cstm_row mt-5 pt-5">
      <div className="col cstm_col">
        <p>{current_date}</p>
        <button type="button" className="btn btn-outline-primary" onClick={button1}>
          Button 1
        </button>
      </div>
      <div className="col cstm_col">
        <p>{patient_name}</p>
        <button type="button" className="btn btn-outline-primary" onClick={button2}>
          Button 2
        </button>
      </div>
    </div>
  );
}

export default App;
