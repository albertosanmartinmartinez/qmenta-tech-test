# Alberto Sanmartin Qmenta Technical Test

A little modification ot technical test to ensure show maximum skills as FastAPI and ReactJS.
The Back-End has two endpoints, one for current date when info is requested and the other for read dicom file patient's name. The Front-End shows two buttons, one for each feature and request the API info and show it.

# Commands to run the project

- $ git clone https://gitlab.com/albertosanmartinmartinez/qmenta-tech-test.git
- $ cd qmenta-tech-test/
- $ docker-compose -f docker-compose.yml up

# Commands to test the project. (Change main.py logging imports)

- $ docker exec -it qmenta_fastapi_ctnr sh
- $ pytest

# Back-End endpoints

- http://0.0.0.0:8000/docs
- http://0.0.0.0:8000/button1
- http://0.0.0.0:8000/button2

# Front-End endpoints

- http://0.0.0.0:3000

# Links of interest & documentation.

- https://pydicom.github.io/pydicom/stable/index.html
- https://getbootstrap.com 
- https://fastapi.tiangolo.com
- https://reactjs.org
